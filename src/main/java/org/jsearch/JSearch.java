package org.jsearch;

import org.jsearch.core.Query;
import org.jsearch.core.Result;
import org.jsearch.process.QueryProcessor;

import java.util.Scanner;
import java.util.logging.Logger;

public class JSearch {
    public static final Logger logger = Logger.getLogger("JSearch");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter Query: ");
        String query = scanner.nextLine();
        Query q = QueryProcessor.process(query);
        for (Result result : q.getResults()) {
            System.out.println("Location:" + result.getLocation() + "\nSummary: " + result.getSummary());
        }
    }
}
