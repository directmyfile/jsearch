package org.jsearch.process;

import org.jsearch.core.Query;

public class QueryProcessor {

    public static Query process(String string) {
        return new Query(string);
    }
}
