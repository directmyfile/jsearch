package org.jsearch.process;

import org.jsearch.core.Query;
import org.jsearch.core.Result;
import org.jsearch.search.PersonSearch;
import org.jsearch.search.Searcher;

import java.util.ArrayList;

public class ResultProcessor {
    private static Class[] searchers = new Class[] {PersonSearch.class};
    public static Result[] process(Query query) {
        String q = query.getQuery();
        ArrayList<Result> results = new ArrayList<Result>();
        for (Class clazz : searchers) {
            Searcher searcher = null;
            try {
                searcher = (Searcher) clazz.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (searcher != null) {
                ArrayList<Result> resultSearched =  searcher.search(q);
                results.addAll(resultSearched);
            }
        }
        return results.toArray(new Result[results.size()]);
    }
}
