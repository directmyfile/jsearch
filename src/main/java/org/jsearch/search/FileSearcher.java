package org.jsearch.search;

import org.jsearch.JSearch;
import org.jsearch.core.Result;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class FileSearcher implements Searcher {
    private ArrayList<Result> results = new ArrayList<Result>();
    private String query;

    @Override
    public ArrayList<Result> search(String query) {
        this.query = query;
        JSearch.logger.info("Starting Home Directory Search");
        File homeDirectory = new File(System.getProperty("user.home"));
        // searchDirectory(homeDirectory);
        return results;
    }

    public void searchDirectory(File dir) {
        JSearch.logger.info("Searching Directory: " + dir.getAbsolutePath());
        File[] files = dir.listFiles();
        if (files==null) {
            return;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                searchDirectory(file);
                continue;
            }
            if (file.getName().matches("/gradle/")) {
                JSearch.logger.info("Found Matching Result!");
                try {
                    results.add(new Result(file.toURI().toURL().toString(), "The FileName matched the Query"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
