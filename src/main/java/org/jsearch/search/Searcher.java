package org.jsearch.search;

import org.jsearch.core.Result;

import java.util.ArrayList;

public interface Searcher {
    public ArrayList<Result> search(String query);
}
