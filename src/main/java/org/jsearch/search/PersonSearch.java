package org.jsearch.search;

import org.jsearch.core.Result;

import java.util.ArrayList;

public class PersonSearch implements Searcher {

    @Override
    public ArrayList<Result> search(String query) {
        ArrayList<Result> results = new ArrayList<Result>();
        boolean isPerson = true;
        String[] terms = query.split(" ");
        boolean[] termCase = new boolean[terms.length];
        int count = 0;
        for (String term : terms) {
            Character firstChar = term.charAt(0);
            termCase[count] = firstChar.toString().matches("[A-Z]");
            count++;
        }
        for (boolean termC : termCase) {
            if (!termC) {
                isPerson = false;
                break;
            }
        }
        if (isPerson) {
            results.add(new Result(query, "The Query is a name more than likely."));
        }
        return results;
    }
}
