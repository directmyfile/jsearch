package org.jsearch.core;

public class Result {
    private String location;
    private String summary;

    public Result(String location, String summary) {
        this.location = location;
        this.summary = summary;
    }

    public String getLocation() {
        return location;
    }

    public String getSummary() {
        return summary;
    }
}
