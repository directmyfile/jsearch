package org.jsearch.core;

import org.jsearch.process.ResultProcessor;

public class Query {
    private String query;
    private Result[] results;

    public Query(String query) {
        this.query = query;
        this.results = ResultProcessor.process(this);
    }

    public String getQuery() {
        return query;
    }

    public Result[] getResults() {
        return results;
    }
}
