package org.jsearch.know;

public class Person extends Entity {
    private String name;

    public Person(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
